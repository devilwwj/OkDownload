package com.duff.download.demo;

import android.app.Application;

import com.duff.download.okdownload.DownloadConfiguration;

/**
 * Author: duff
 * Date: 2017/11/6
 * Version: 1.0.0
 */

public class DemoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DownloadConfiguration.Builder builder = new DownloadConfiguration.Builder()
                .dbName("download_demo.db")
                .debug(true)
                .tag("OkDownload");
        DownloadConfiguration.init(builder);
    }

}
