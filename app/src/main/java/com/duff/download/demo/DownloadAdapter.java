package com.duff.download.demo;

import android.content.Context;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.duff.download.okdownload.AbsDownloadManager;
import com.duff.download.okdownload.DownloadError;
import com.duff.download.okdownload.DownloadManager;
import com.duff.download.okdownload.DownloadStatus;
import com.duff.download.okdownload.model.DownloadInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dingfeng on 2017/8/25.
 */

public class DownloadAdapter extends BaseAdapter {

    private Context mContext;
    private List<DownloadInfo> mDownloadList = new ArrayList<>();

    public DownloadAdapter(Context context, List<DownloadInfo> list) {
        mContext = context;
        mDownloadList = list;
    }

    @Override
    public int getCount() {
        return mDownloadList.size();
    }

    @Override
    public DownloadInfo getItem(int position) {
        return mDownloadList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mDownloadList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_download_list, null, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final DownloadInfo info = mDownloadList.get(position);
        holder.mName.setText(info.getTaskName());
        holder.mProTxt.setText(String.format("%s/%s", Formatter.formatFileSize(mContext, info.getDownloadedBytes())
                , Formatter.formatFileSize(mContext, info.getTotalSize())));
        holder.mProgress.setMax(100);
        holder.mProgress.setProgress(info.getTotalSize() == 0 ? 0 : (int) (100 * info.getDownloadedBytes() / info.getTotalSize()));

        String statusHint = "";
        switch (info.getStatus()) {
            case DownloadStatus.STATUS_STARTED:
                statusHint = "下载中..";
                break;
            case DownloadStatus.STATUS_ERROR:
                statusHint = "失败";
                break;
            case DownloadStatus.STATUS_DOWNLOAD_COMPLETED:
                statusHint = "完成";
                break;
            case DownloadStatus.STATUS_PAUSED:
                statusHint = "暂停";
                break;
            case DownloadStatus.STATUS_PENDED:
                statusHint = "等待";
                break;
            default:
                statusHint = "准备中..";
                break;
        }
        holder.mStatus.setText(statusHint);

        return convertView;
    }

    class ViewHolder {
        TextView mName;
        TextView mProTxt;
        ProgressBar mProgress;
        TextView mStatus;

        public ViewHolder(View parent) {
            mName = (TextView) parent.findViewById(R.id.download_name);
            mProTxt = (TextView) parent.findViewById(R.id.download_progress_txt);
            mProgress = (ProgressBar) parent.findViewById(R.id.download_progress);
            mStatus = (TextView) parent.findViewById(R.id.download_status);
        }
    }

}
