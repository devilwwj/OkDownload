package com.duff.download.demo;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.duff.download.okdownload.OkDownload;
import com.duff.download.okdownload.async.ParallelScheduler;
import com.duff.download.okdownload.async.Task;
import com.duff.download.okdownload.interfaces.OnDownloadListener;
import com.duff.download.okdownload.utils.Utils;

import java.io.File;

/**
 * Author: duff
 * Date: 2018/10/22
 * Version: 1.0.0
 * Description:
 */
public class SyncDownloadActivity extends AppCompatActivity implements View.OnClickListener {

    private String DOWNLOAD_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/aaa/";

    private final String[] urls = new String[]{
            "http://imgsrc.baidu.com/image/c0%3Dshijue1%2C0%2C0%2C294%2C40/sign=9909ddc1b91c8701c2bbbaa54f16f45a/a08b87d6277f9e2fb0727e2d1530e924b899f354.jpg",
            "http://f3.market.xiaomi.com/download/AppStore/08c674eee0f857ed80f0efa9158a6cb021c42957b/com.imibaby.client.apk",
            "http://imgsrc.baidu.com/image/c0%3Dshijue1%2C0%2C0%2C294%2C40/sign=ae4e87268d94a4c21e2eef68669d71a0/7c1ed21b0ef41bd5d5a88edd5bda81cb39db3d1b.jpg"
    };


    private Button bt_download;
    private TextView download_progress_txt;
    private ProgressBar download_progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synce_download);
        bt_download = (Button) findViewById(R.id.bt_download);
        bt_download.setOnClickListener(this);
        download_progress_txt = (TextView) findViewById(R.id.download_progress_txt);
        download_progress = (ProgressBar) findViewById(R.id.download_progress);
        download_progress.setMax(100);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_download:
                download();
                break;
        }
    }

    private void download() {
        ParallelScheduler.execute(new Task<Object, File>(null) {// 同步下载请在子线程中执行
            @Override
            public File doInBackground(Object input) {
//                return OkDownload.downloadSync(urls[0], DOWNLOAD_DIR + Utils.getFileName(urls[0]));
                return OkDownload.downloadSync(urls[1], DOWNLOAD_DIR + Utils.getFileName(urls[1]), new OnDownloadListener() {
                    @Override
                    public void onStart(String url) {

                    }

                    @Override
                    public void onError(String url, Exception e) {

                    }

                    @Override
                    public void onProgress(String url, long downloadedBytes, long totalBytes, File file) {
                        download_progress_txt.setText(String.format("%s/%s", Formatter.formatFileSize(SyncDownloadActivity.this, downloadedBytes)
                                , Formatter.formatFileSize(SyncDownloadActivity.this, totalBytes)));
                        download_progress.setProgress(downloadedBytes == 0 ? 0 : (int) (100 * downloadedBytes / totalBytes));
                    }

                    @Override
                    public void onSuccess(String url, File file) {

                    }
                });
            }

        });
    }

}
