package com.duff.download.okdownload.async;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 串行异步任务调度器
 *
 * @author: duff
 * @date: 2019/3/30
 * @since: 1.0.0
 */
public class SerialScheduler extends Scheduler {

    private static ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    public static void execute(Runnable task) {
        mExecutor.execute(task);
    }

    public static <Params, Result> void execute(final Task<Params, Result> task) {
        mExecutor.execute(new AsyncTask(task));
    }

    public static <Params, Result> Future<Result> submit(final Task<Params, Result> task) {
        return mExecutor.submit(new Callable<Result>(){
            @Override
            public Result call() throws Exception {
                return task.doInBackground(task.mParams);
            }
        });
    }

}
