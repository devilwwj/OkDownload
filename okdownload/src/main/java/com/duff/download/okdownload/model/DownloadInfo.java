package com.duff.download.okdownload.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.duff.download.okdownload.DownloadError;
import com.duff.download.okdownload.DownloadStatus;
import com.duff.download.okdownload.database.DownloadColumns;

/**
 * author：duff
 * version：1.0.0
 * date：2017/8/27
 */
public class DownloadInfo implements Parcelable {

    public static final int ID_INVALID = 0;

    /**
     * 自动生成，区别不同的下载任务
     */
    private long mId = ID_INVALID;
    private String mUrl;//下载url
    private String mTag;//标签
    private String mPath;//本地保存路径
    private int mPriority = android.os.Process.THREAD_PRIORITY_BACKGROUND;//任务优先级
    private int mStatus = DownloadStatus.STATUS_PENDED;//下载状态
    private long mTotalSize = 0;//总大小
    private long mDownloadedBytes = 0;//已下载
    private int mErrorCode = DownloadError.ERROR_SUCCESS;//下载错误码
    private long mAddTime = 0;//任务添加时间
    private long mModifyTime = 0;//任务更新时间
    private String mTaskName;//任务名
    private String mFileName;//文件名
    private int mHttpStatus;//http状态码
    private String mMD5;//MD5码
    private String mExtra1;//扩展字段1
    private String mExtra2;//扩展字段2
    private String mExtra3;//扩展字段3

    public DownloadInfo() {

    }

    public DownloadInfo(Parcel in) {
        mId = in.readLong();
        mUrl = in.readString();
        mTag = in.readString();
        mPath = in.readString();
        mPriority = in.readInt();
        mStatus = in.readInt();
        mTotalSize = in.readLong();
        mDownloadedBytes = in.readLong();
        mErrorCode = in.readInt();
        mAddTime = in.readLong();
        mModifyTime = in.readLong();
        mTaskName = in.readString();
        mFileName = in.readString();
        mHttpStatus = in.readInt();
        mMD5 = in.readString();
        mExtra1 = in.readString();
        mExtra2 = in.readString();
        mExtra3 = in.readString();
    }

    public DownloadInfo(ContentValues contentValues) {
        mId = contentValues.getAsLong(DownloadColumns.COLUMN_TASK_ID);
        mUrl = contentValues.getAsString(DownloadColumns.COLUMN_TASK_URL);
        mTag = contentValues.getAsString(DownloadColumns.COLUMN_TASK_TAG);
        mPath = contentValues.getAsString(DownloadColumns.COLUMN_TASK_PATH);
        mPriority = contentValues.getAsInteger(DownloadColumns.COLUMN_TASK_PRIORITY);
        mStatus = contentValues.getAsInteger(DownloadColumns.COLUMN_TASK_STATUS);
        mTotalSize = contentValues.getAsLong(DownloadColumns.COLUMN_TASK_TOTAL);
        mDownloadedBytes = contentValues.getAsLong(DownloadColumns.COLUMN_TASK_DOWNLOADED);
        mErrorCode = contentValues.getAsInteger(DownloadColumns.COLUMN_TASK_ERROR);
        mAddTime = contentValues.getAsLong(DownloadColumns.COLUMN_TASK_ADD_TIME);
        mModifyTime = contentValues.getAsLong(DownloadColumns.COLUMN_TASK_MODIFY_TIME);
        mTaskName = contentValues.getAsString(DownloadColumns.COLUMN_TASK_NAME);
        mFileName = contentValues.getAsString(DownloadColumns.COLUMN_TASK_FILENAME);
        mHttpStatus = contentValues.getAsInteger(DownloadColumns.COLUMN_TASK_HTTP_STATUS);
        mMD5 = contentValues.getAsString(DownloadColumns.COLUMN_TASK_MD5);
        mExtra1 = contentValues.getAsString(DownloadColumns.COLUMN_TASK_EXTRA_1);
        mExtra2 = contentValues.getAsString(DownloadColumns.COLUMN_TASK_EXTRA_2);
        mExtra3 = contentValues.getAsString(DownloadColumns.COLUMN_TASK_EXTRA_3);
    }

    public DownloadInfo(Cursor cursor) {
        mId = cursor.getLong(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_ID));
        mUrl = cursor.getString(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_URL));
        mTag = cursor.getString(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_TAG));
        mPath = cursor.getString(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_PATH));
        mPriority = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_PRIORITY));
        mStatus = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_STATUS));
        mTotalSize = cursor.getLong(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_TOTAL));
        mDownloadedBytes = cursor.getLong(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_DOWNLOADED));
        mErrorCode = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_ERROR));
        mAddTime = cursor.getLong(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_ADD_TIME));
        mModifyTime = cursor.getLong(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_MODIFY_TIME));
        mTaskName = cursor.getString(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_NAME));
        mFileName = cursor.getString(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_FILENAME));
        mHttpStatus = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_HTTP_STATUS));
        mMD5 = cursor.getString(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_MD5));
        mExtra1 = cursor.getString(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_EXTRA_1));
        mExtra2 = cursor.getString(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_EXTRA_2));
        mExtra3 = cursor.getString(cursor.getColumnIndexOrThrow(DownloadColumns.COLUMN_TASK_EXTRA_3));
    }

    public static final Parcelable.Creator<DownloadInfo> CREATOR
            = new Parcelable.Creator<DownloadInfo>() {
        public DownloadInfo createFromParcel(Parcel in) {
            return new DownloadInfo(in);
        }

        public DownloadInfo[] newArray(int size) {
            return new DownloadInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mUrl);
        dest.writeString(mTag);
        dest.writeString(mPath);
        dest.writeInt(mPriority);
        dest.writeInt(mStatus);
        dest.writeLong(mTotalSize);
        dest.writeLong(mDownloadedBytes);
        dest.writeInt(mErrorCode);
        dest.writeLong(mAddTime);
        dest.writeLong(mModifyTime);
        dest.writeString(mTaskName);
        dest.writeString(mFileName);
        dest.writeInt(mHttpStatus);
        dest.writeString(mMD5);
        dest.writeString(mExtra1);
        dest.writeString(mExtra2);
        dest.writeString(mExtra3);
    }

    public ContentValues convert2ContentValues() {
        ContentValues values = new ContentValues();
        values.put(DownloadColumns.COLUMN_TASK_ID, mId);
        values.put(DownloadColumns.COLUMN_TASK_URL, mUrl);
        values.put(DownloadColumns.COLUMN_TASK_TAG, mTag);
        values.put(DownloadColumns.COLUMN_TASK_PATH, mPath);
        values.put(DownloadColumns.COLUMN_TASK_PRIORITY, mPriority);
        values.put(DownloadColumns.COLUMN_TASK_STATUS, mStatus);
        values.put(DownloadColumns.COLUMN_TASK_TOTAL, mTotalSize);
        values.put(DownloadColumns.COLUMN_TASK_DOWNLOADED, mDownloadedBytes);
        values.put(DownloadColumns.COLUMN_TASK_ERROR, mErrorCode);
        values.put(DownloadColumns.COLUMN_TASK_ADD_TIME, mAddTime);
        values.put(DownloadColumns.COLUMN_TASK_MODIFY_TIME, mModifyTime);
        values.put(DownloadColumns.COLUMN_TASK_NAME, mTaskName);
        values.put(DownloadColumns.COLUMN_TASK_FILENAME, mFileName);
        values.put(DownloadColumns.COLUMN_TASK_HTTP_STATUS, mHttpStatus);
        values.put(DownloadColumns.COLUMN_TASK_MD5, mMD5);
        values.put(DownloadColumns.COLUMN_TASK_EXTRA_1, mExtra1);
        values.put(DownloadColumns.COLUMN_TASK_EXTRA_2, mExtra2);
        values.put(DownloadColumns.COLUMN_TASK_EXTRA_3, mExtra3);

        return values;
    }

    public static DownloadInfo from(ContentValues contentValues) {
        return new DownloadInfo(contentValues);
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public String getTag() {
        return mTag;
    }

    public void setTag(String tag) {
        this.mTag = tag;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        this.mPath = path;
    }

    public int getPriority() {
        return mPriority;
    }

    public void setPriority(int priority) {
        this.mPriority = priority;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int status) {
        this.mStatus = status;
    }

    public long getTotalSize() {
        return mTotalSize;
    }

    public void setTotalSize(long totalSize) {
        this.mTotalSize = totalSize;
    }

    public long getDownloadedBytes() {
        return mDownloadedBytes;
    }

    public void setDownloadedBytes(long downloadedBytes) {
        this.mDownloadedBytes = downloadedBytes;
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(int errorCode) {
        this.mErrorCode = errorCode;
    }

    public long getAddTime() {
        return mAddTime;
    }

    public void setAddTime(long addTime) {
        this.mAddTime = addTime;
    }

    public long getModifyTime() {
        return mModifyTime;
    }

    public void setModifyTime(long modifyTime) {
        this.mModifyTime = modifyTime;
    }

    public String getTaskName() {
        return mTaskName;
    }

    public void setTaskName(String taskName) {
        this.mTaskName = taskName;
    }

    public String getFileName() {
        return mFileName;
    }

    public void setFileName(String fileName) {
        this.mFileName = fileName;
    }

    public int getHttpStatus() {
        return mHttpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.mHttpStatus = httpStatus;
    }

    public String getMD5() {
        return mMD5;
    }

    public void setMD5(String mD5) {
        this.mMD5 = mD5;
    }

    public String getExtra1() {
        return mExtra1;
    }

    public void setExtra1(String extra1) {
        this.mExtra1 = extra1;
    }

    public String getExtra2() {
        return mExtra2;
    }

    public void setExtra2(String extra2) {
        this.mExtra2 = extra2;
    }

    public String getExtra3() {
        return mExtra3;
    }

    public void setExtra3(String extra3) {
        this.mExtra3 = extra3;
    }

}
