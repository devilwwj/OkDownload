package com.duff.download.okdownload.database.operator;

public class QueryParameter {

    private String mConditionStr;
    private String[] mArgs;
    private OrderBy mOrderBy;
    private GroupBy mGroupBy;
    private Having mHaving;

    public QueryParameter() {
    }

    public QueryParameter setCondition(Condition condition) {
        mConditionStr = condition.getConditionString();
        mArgs = condition.getConditionArgs();
        return this;
    }

    public String getCondition() {
        return mConditionStr;
    }


    public QueryParameter setOrderBy(OrderBy orderBy) {
        mOrderBy = orderBy;
        return this;
    }

    public QueryParameter setHaving(Having having) {
        mHaving = having;
        return this;
    }

    public QueryParameter setGroupBy(GroupBy groupBy) {
        mGroupBy = groupBy;
        return this;
    }

    public String getGroupBy() {
        return mGroupBy == null ? null : mGroupBy.toString();
    }

    public String getHaving() {
        return mHaving == null ? null : mHaving.toString();
    }

    public String getOrderBy() {
        return mOrderBy == null ? null : mOrderBy.toString();
    }

    public String[] getArgs() {
        return mArgs;
    }
}
