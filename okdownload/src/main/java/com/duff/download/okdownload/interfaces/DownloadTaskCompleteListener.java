package com.duff.download.okdownload.interfaces;

/**
 * 下载引擎中无下载任务时回调
 *
 * Author: duff
 * Date: 2018/8/7
 * Version: 1.0.0
 */
public interface DownloadTaskCompleteListener {
    void onComplete();
}
