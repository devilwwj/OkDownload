package com.duff.download.okdownload.database.operator;

public class OperatorFactory {

    public static Operator equal() {
        return new Operator.Equal();
    }

    public static Operator notEqual() {
        return new Operator.NotEqual();
    }

    public static Operator like() {
        return new Operator.Like();
    }

    public static Operator between() {
        return new Operator.Between();
    }

    public static Operator in(int count) {
        return new Operator.In(count);
    }

    public static Operator notIn(int count) {
        return new Operator.NotIn(count);
    }

    public static Operator greatThan() {
        return new Operator.GreatThan();
    }

    public static Operator lessThan() {
        return new Operator.LessThan();
    }

    public static Operator greatEqual() {
        return new Operator.GreatEqual();
    }

    public static Operator lessEqual() {
        return new Operator.LessEqual();
    }

    public static Operator isNull() {
        return new Operator.IsNull();
    }

    public static Operator isNotNull() {
        return new Operator.IsNotNull();
    }
}
