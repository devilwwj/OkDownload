package com.duff.download.okdownload.database.operator;

public class GroupBy {

    private String[] mColumns;

    public GroupBy(String...columns) {
        mColumns = columns;
    }

    @Override
    public String toString() {
        if (mColumns == null || mColumns.length == 0) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder("");
        for (int i = 0; i < mColumns.length; i++) {
            stringBuilder.append(mColumns[i]);
            if (i < mColumns.length - 1) {
                stringBuilder.append(",");
            }
        }
        return stringBuilder.toString();
    }
}
