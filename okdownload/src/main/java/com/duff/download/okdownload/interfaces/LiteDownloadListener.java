package com.duff.download.okdownload.interfaces;

import java.io.File;

/**
 * Author: duff
 * Date: 2018/7/25
 * Version: 1.0.0
 * Description: Lite download listener.
 */
public interface LiteDownloadListener {
    void onError(Exception e);

    void onSuccess(File file);
}
