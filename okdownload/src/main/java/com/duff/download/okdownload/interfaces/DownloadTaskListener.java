package com.duff.download.okdownload.interfaces;

/**
 * author：duff
 * version：1.0.0
 * date：2017/8/27
 * description：下载任务监听器
 */
public interface DownloadTaskListener {
    void onStart(long taskId, long downloadBytes, long totalBytes);

    void onProgress(long taskId, long downloadBytes, long totalBytes);

    void onCanceled(long taskId, long downloadBytes, long totalBytes);

    void onError(long taskId, int httpStatus, int errorCode, String msg);

    void onFinished(long taskId, long downloadBytes, long totalBytes);
}
