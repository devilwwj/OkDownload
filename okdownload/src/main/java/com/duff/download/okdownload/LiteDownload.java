package com.duff.download.okdownload;

import com.duff.download.okdownload.async.SerialScheduler;
import com.duff.download.okdownload.async.Task;
import com.duff.download.okdownload.interfaces.DownloadTaskCompleteListener;
import com.duff.download.okdownload.interfaces.LiteDownloadListener;
import com.duff.download.okdownload.interfaces.OnDownloadListener;
import com.duff.download.okdownload.utils.Logger;
import com.duff.download.okdownload.utils.Utils;

import java.io.File;
import java.util.List;

import static com.duff.download.okdownload.DownloadManager.DOWNLOAD_SUFFIX;

/**
 * Author: duff
 * Date: 2018/7/25
 * Version: 1.0.0
 * Description: Light download, only call back onSuccess & onFail
 */
public class LiteDownload {

    public static void download(final String url, final String path, final LiteDownloadListener listener) {
        OkDownload.download(url, path, new OnDownloadListener() {
            @Override
            public void onStart(String url) {
            }

            @Override
            public void onError(String url, Exception e) {
                if (listener != null) {
                    listener.onError(e);
                }
            }

            @Override
            public void onProgress(String url, long downloadedBytes, long totalBytes, File file) {
            }

            @Override
            public void onSuccess(String url, File file) {
                if (listener != null) {
                    listener.onSuccess(file);
                }
            }
        });
    }

    public static void download(final boolean deleteExist, final String[] urls, final String[] paths, final DownloadTaskCompleteListener l) {
        if (urls.length == 0 || urls.length != paths.length) {
            Logger.d("urls length is not equals paths");
            return;
        }

        final List<String> urlList = Utils.stringArray2list(urls);
        final List<String> pathList = Utils.stringArray2list(paths);

        final DownloadEngine downloadEngine = new DownloadEngine();
        downloadEngine.setMaxRunningTasks(1);

        // 检查重复, 并删除已存在的文件
        SerialScheduler.execute(new Task<String[], Boolean>(paths) {
            @Override
            public Boolean doInBackground(String[] strings) {
                for (int i = urls.length - 1; i >= 0; i--) {
                    String path = paths[i];
                    File downloadFile = new File(path);
                    if (downloadFile.exists()) {
                        if (deleteExist) {
                            downloadFile.delete();
                        } else {
                            urlList.remove(i);
                            pathList.remove(i);
                        }
                    }
                    String tmpPath = path + "." + DOWNLOAD_SUFFIX;
                    File tmpFile = new File(tmpPath);
                    if (tmpFile.exists()) {
                        tmpFile.delete();
                    }
                }

                return true;
            }

            @Override
            public void onPostExecute(Boolean result) {
                if (result) {
                    if (deleteExist) {
                        downloadEngine.addTask(urls, paths, l);
                    } else {
                        if (urlList.size() > 0) {
                            downloadEngine.addTask(Utils.list2StringArray(urlList), Utils.list2StringArray(pathList), l);
                        } else {
                            if (l != null) l.onComplete();
                        }
                    }
                }
            }
        });
    }

}
