package com.duff.download.okdownload.database;

import android.provider.BaseColumns;

/**
 * author：duff
 * version：1.0.0
 * date：2017/8/27
 */
public class DownloadColumns {
    public static final String COLUMN_TASK_ID = BaseColumns._ID;
    public static final String COLUMN_TASK_TAG = "task_id";
    public static final String COLUMN_TASK_URL = "task_url";
    public static final String COLUMN_TASK_PRIORITY = "task_priority";
    public static final String COLUMN_TASK_STATUS = "task_status";
    public static final String COLUMN_TASK_TOTAL = "total_bytes";
    public static final String COLUMN_TASK_DOWNLOADED = "downloaded_bytes";
    public static final String COLUMN_TASK_PATH = "file_path";
    public static final String COLUMN_TASK_NAME = "target_name";
    public static final String COLUMN_TASK_ERROR = "error_code";
    public static final String COLUMN_TASK_HTTP_STATUS = "http_status";
    public static final String COLUMN_TASK_FILENAME = "task_filename";
    public static final String COLUMN_TASK_ADD_TIME = "task_timestamp";
    public static final String COLUMN_TASK_MODIFY_TIME = "task_modify_time";
    public static final String COLUMN_TASK_MD5 = "_md5";

    // 扩展字段
    public static final String COLUMN_TASK_EXTRA_1 = "extra_info_1";
    public static final String COLUMN_TASK_EXTRA_2 = "extra_info_2";
    public static final String COLUMN_TASK_EXTRA_3 = "extra_info_3";

    public static final String[] COLUMNS = new String[] {
            COLUMN_TASK_ID
            , COLUMN_TASK_TAG
            , COLUMN_TASK_URL
            , COLUMN_TASK_PRIORITY
            , COLUMN_TASK_STATUS
            , COLUMN_TASK_TOTAL
            , COLUMN_TASK_DOWNLOADED
            , COLUMN_TASK_PATH
            , COLUMN_TASK_NAME
            , COLUMN_TASK_ERROR
            , COLUMN_TASK_HTTP_STATUS
            , COLUMN_TASK_FILENAME
            , COLUMN_TASK_ADD_TIME
            , COLUMN_TASK_MODIFY_TIME
            , COLUMN_TASK_MD5
            , COLUMN_TASK_EXTRA_1
            , COLUMN_TASK_EXTRA_2
            , COLUMN_TASK_EXTRA_3
    };
}
