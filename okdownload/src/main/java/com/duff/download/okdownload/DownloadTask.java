package com.duff.download.okdownload;

import android.os.Handler;
import android.os.Looper;

import com.duff.download.okdownload.interfaces.DownloadTaskListener;
import com.duff.download.okdownload.utils.FileUtils;
import com.duff.download.okdownload.utils.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * author：duff
 * version：1.0.0
 * date：2017/8/27
 * description：下载任务，一个DownloadTask对应一个下载任务
 */
public class DownloadTask {

    private static final String TAG = "DownloadTask";

    private static final Handler mHandler = new Handler(Looper.getMainLooper());

    /**
     * 下载进度调用间隔，防止频繁的UI刷新操作
     */
    public static final long INTERVAL_PROGRESS = 100;

    private long mProgressStart = 0;

    private long mTaskId; // 任务id， 唯一
    private String mUrl; // 下载地址
    private String mLocalPath; // 本地保存路径
    private long mRangeOffset; // 下载偏移，用于断点续传

    private OkDownload mOkDownload;
    private DownloadTaskListener mDownloadTaskListener;
    private long mDownloadBytes; // 已下载
    private long mTotalBytes; // 总大小
    private volatile boolean mIsCanceled = false;

    public DownloadTask(long taskId, String url, String localPath, long rangeOffset, DownloadTaskListener listener) {
        Logger.d(TAG, "taskId:" + taskId + "  url:" + url + "  localPath:" + localPath + "  rangeOffset:" + rangeOffset);
        mTaskId = taskId;
        mUrl = url;
        mLocalPath = localPath;
        mRangeOffset = rangeOffset;
        mDownloadTaskListener = listener;

        // create file
        FileUtils.createFile(localPath);

        mOkDownload = new OkDownload.Builder()
                .url(mUrl)
                .tag(mTaskId)
                .isAppend(true) // 支持断点续传
                .header("Range", "bytes=" + mRangeOffset + "-")
                .build();
    }

    public void setDownloadTaskListener(DownloadTaskListener listener) {
        mDownloadTaskListener = listener;
    }

    /**
     * Execute task download.
     */
    public void execute() {
        mOkDownload.download(mOkDownloadCallback);
    }

    private Callback mOkDownloadCallback = new Callback() {
        @Override
        public void onFailure(Call call, IOException e) {
            notifyError(mTaskId, -1, DownloadError.ERROR_NETWORK, e != null ? e.toString() : DownloadError.NAMES.get(DownloadError.ERROR_NETWORK));
        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            if (response != null) {
                if (response.code() >= 300) {
                    notifyError(mTaskId, response.code(), DownloadError.ERROR_NETWORK, DownloadError.NAMES.get(DownloadError.ERROR_NETWORK));
                    if (response.body() != null) {
                        response.body().close();
                    }
                    return;
                }
                long contentLength = Math.max(0, mRangeOffset + response.body().contentLength());
                long count = mRangeOffset;
                InputStream inStream = response.body().byteStream();
                FileOutputStream buffer = null;
                try {
                    // start
                    notifyStart(mTaskId, count, contentLength);

                    File targetFile = new File(mLocalPath);
                    if (!targetFile.exists()) {
                        FileUtils.createFile(targetFile.getAbsolutePath());
                    }
                    buffer = new FileOutputStream(targetFile, mOkDownload.isAppend());
                    if (inStream != null) {
                        byte[] buf = new byte[OkDownload.DOWNLOAD_BUFFER_SIZE];
                        int l;
                        while (!Thread.currentThread().isInterrupted()) {
                            if (mIsCanceled) {
                                mOkDownload.cancel();
                                break;
                            }
                            l = inStream.read(buf);
                            if (l == -1) {// 写入结束
                                break;
                            }

                            count += l;
                            buffer.write(buf, 0, l);

                            mTotalBytes = contentLength <= 0 ? count : contentLength;
                            mDownloadBytes = count;
                            mRangeOffset = count;

                            if ((System.currentTimeMillis() - mProgressStart) >= INTERVAL_PROGRESS) {
                                notifyProgress(mTaskId, count, mTotalBytes);
                                mProgressStart = System.currentTimeMillis();
                            }
                        }
                        buffer.flush();
                    }
                } catch (Exception e) {
                    notifyError(mTaskId, response.code(), DownloadError.ERROR_IO, e.getMessage());
                    e.printStackTrace();
                } finally {
                    try {
                        if (response.body() != null) {
                            response.body().close();
                        }
                        if (buffer != null) {
                            buffer.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    notifyFinished(mTaskId, count, mTotalBytes);
                }
            }
        }
    };

    public void cancel() {
        mIsCanceled = true;
        notifyCancel(mTaskId, mDownloadBytes, mTotalBytes);
    }

    public long getTaskId() {
        return mTaskId;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getLocalPath() {
        return mLocalPath;
    }

    private void notifyStart(final long taskId, final long downloadBytes, final long totalBytes) {
        if (mDownloadTaskListener != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mDownloadTaskListener.onStart(taskId, downloadBytes, totalBytes);
                }
            });
        }
    }

    private void notifyProgress(long taskId, long downloadBytes, long totalBytes) {
        if (mDownloadTaskListener != null) {
            mDownloadTaskListener.onProgress(taskId, downloadBytes, totalBytes);
        }
    }

    private void notifyCancel(final long taskId, final long downloadBytes, final long totalBytes) {
        if (mDownloadTaskListener != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mDownloadTaskListener.onCanceled(taskId, downloadBytes, totalBytes);
                }
            });
        }
    }

    private void notifyError(final long taskId, final int httpStatus, final int errorCode, final String msg) {
        if (mDownloadTaskListener != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mDownloadTaskListener.onError(taskId, httpStatus, errorCode, msg);
                }
            });
        }
    }

    private void notifyFinished(final long taskId, final long downloadBytes, final long totalBytes) {
        if (mDownloadTaskListener != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mDownloadTaskListener.onFinished(taskId, downloadBytes, totalBytes);
                }
            });
        }
    }

}
