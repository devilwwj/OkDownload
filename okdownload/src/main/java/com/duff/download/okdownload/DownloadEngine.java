package com.duff.download.okdownload;

import com.duff.download.okdownload.interfaces.DownloadTaskCompleteListener;
import com.duff.download.okdownload.interfaces.DownloadTaskListener;
import com.duff.download.okdownload.utils.Logger;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 下载引擎，下载任务调度
 *
 * author：duff
 * version：1.0.0
 * date：2017/8/27
 */
public class DownloadEngine {
    private static final int DEFAULT_MAX_RUNNING_TASKS = 3; // 默认最大下载任务
    private int mMaxRunningTasks = DEFAULT_MAX_RUNNING_TASKS; // 最大下载任务

    private DownloadTaskListener mDownloadTaskListener;

    private DownloadTaskCompleteListener mDownloadTaskCompleteListener;

    /**
     * 暂存队列，如果正在执行的任务队列已是最大可执行任务数{@link #mMaxRunningTasks}，就将新添加进的任务放入到该暂存队列中
     */
    private Queue<DownloadTask> mTemporaryTasks = new ConcurrentLinkedQueue<>();

    /**
     * 正在执行的任务队列
     */
    private Queue<DownloadTask> mActiveTasks = new ConcurrentLinkedQueue<>();

    public void setMaxRunningTasks(int max) {
        if (max > 0) {
            mMaxRunningTasks = max;
        }
    }

    public void addTask(long taskId, String url, String path, long rangeOffset, DownloadTaskListener listener) {
        Logger.d("taskId:" + taskId + "  url:" + url + "  path:" + path + "  rangeOffset:" + rangeOffset);

        mDownloadTaskListener = listener;

        if (!isTemporaryTask(taskId) && !isActiveTask(taskId)) {
            DownloadTask downloadTask = new DownloadTask(taskId, url, path, rangeOffset, mInnerDownloadTaskListener);
            mTemporaryTasks.add(downloadTask);

            scheduleNext();
        }
    }

    public void addTask(String[] urls, String[] paths, DownloadTaskCompleteListener l) {
        for (int i = 0; i < urls.length; i++) {
            DownloadTask downloadTask = new DownloadTask(i, urls[i], paths[i], 0, mInnerDownloadTaskListener);
            mTemporaryTasks.add(downloadTask);
        }
        mDownloadTaskCompleteListener = l;

        scheduleNext();
    }

    private void scheduleNext() {
        if (mActiveTasks.size() < mMaxRunningTasks) {
            DownloadTask downloadTask = mTemporaryTasks.poll();
            if (downloadTask != null) {
                mActiveTasks.add(downloadTask);
                downloadTask.execute();
            } else {
                Logger.d("No download task need to execute.");

                if (mDownloadTaskCompleteListener != null) {
                    mDownloadTaskCompleteListener.onComplete();
                }
            }
        }
    }

    public void cancelTask(long taskId) {
        cancelTemporaryTask(taskId);
        cancelActiveTask(taskId);
    }

    private void cancelTemporaryTask(long taskId) {
        DownloadTask downloadTask = findTemporaryTask(taskId);
        if (downloadTask != null) {
            downloadTask.cancel();
            mTemporaryTasks.remove(downloadTask);
        }
    }

    private void cancelActiveTask(long taskId) {
        DownloadTask downloadTask = findActiveTask(taskId);
        if (downloadTask != null) {
            downloadTask.cancel();
            mActiveTasks.remove(downloadTask);
        }
    }

    private void removeActiveTask(long taskId) {
        DownloadTask downloadTask = findActiveTask(taskId);
        if (downloadTask != null) {
            mActiveTasks.remove(downloadTask);
        }
    }

    private boolean isTemporaryTask(long taskId) {
        return findTemporaryTask(taskId) != null;
    }

    private boolean isActiveTask(long taskId) {
        return findActiveTask(taskId) != null;
    }

    private DownloadTask findTemporaryTask(long taskId) {
        for (DownloadTask task : mTemporaryTasks) {
            if (task.getTaskId() == taskId) {
                return task;
            }
        }
        return null;
    }

    private DownloadTask findActiveTask(long taskId) {
        for (DownloadTask task : mActiveTasks) {
            if (task.getTaskId() == taskId) {
                return task;
            }
        }
        return null;
    }

    private DownloadTaskListener mInnerDownloadTaskListener = new DownloadTaskListener() {

        @Override
        public void onStart(long taskId, long downloadBytes, long totalBytes) {
            if (mDownloadTaskListener != null) {
                mDownloadTaskListener.onStart(taskId, downloadBytes, totalBytes);
            }
        }

        @Override
        public void onProgress(long taskId, long downloadBytes, long totalBytes) {
            if (mDownloadTaskListener != null) {
                mDownloadTaskListener.onProgress(taskId, downloadBytes, totalBytes);
            }
        }

        @Override
        public void onCanceled(long taskId, long downloadBytes, long totalBytes) {
            if (mDownloadTaskListener != null) {
                mDownloadTaskListener.onCanceled(taskId, downloadBytes, totalBytes);
            }
            scheduleNext();
        }

        @Override
        public void onError(long taskId, int httpStatus, int errorCode, String msg) {
            if (mDownloadTaskListener != null) {
                mDownloadTaskListener.onError(taskId, httpStatus, errorCode, msg);
            }
            cancelActiveTask(taskId);
            scheduleNext();
        }

        @Override
        public void onFinished(long taskId, long downloadBytes, long totalBytes) {
            if (mDownloadTaskListener != null) {
                mDownloadTaskListener.onFinished(taskId, downloadBytes, totalBytes);
            }
//            cancelActiveTask(taskId);
            removeActiveTask(taskId);
            scheduleNext();
        }

    };

}
