package com.duff.download.okdownload;

import android.os.Handler;
import android.os.Looper;

import com.duff.download.okdownload.database.operator.QueryParameter;
import com.duff.download.okdownload.async.WorkHandler;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * author：duff
 * version：1.0.0
 * date：2017/8/27
 */
public abstract class AbsDownloadManager<T> {
    protected Handler mHandler = new Handler(Looper.getMainLooper());
    protected WorkHandler mWorkHandler = new WorkHandler();
    protected List<IDownloadObserver> mObservers = new CopyOnWriteArrayList<>();

    public abstract void add(final T item, final AddCallback<T> callback);
    public abstract void add(final AddCallback<T> callback, final T... items);
    public abstract void pause(final long id);
    public abstract void pauseAll();
    public abstract void resume(final long id);
    public abstract void resumeAll();
    public abstract void delete(final boolean onlyDB, final long id, final DeleteCallback<T> callback);
    public abstract void delete(final boolean onlyDB, final DeleteCallback<T> callback, final long... ids);
    public abstract void deleteAll(final DeleteCallback<T> callback);
    public abstract void queryAll(final QueryCallback<T> callback);
    public abstract void query(final QueryParameter parameter, final QueryCallback<T> callback);


    public void registerDownloadObserver(IDownloadObserver observer) {
        if (observer != null && !mObservers.contains(observer)) {
            mObservers.add(observer);
        }
    }

    public void unRegisterDownloadObserver(IDownloadObserver observer) {
        if (observer != null && mObservers.contains(observer)) {
            mObservers.remove(observer);
        }
    }

    protected void notifyUpdateStatus(final T t) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                for (IDownloadObserver observer : mObservers) {
                    observer.onUpdateStatus(t);
                }
            }
        });
    }

    protected void notifyProgress(final T t) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                for (IDownloadObserver observer : mObservers) {
                    observer.onProgress(t);
                }
            }
        });
    }

    public interface AddCallback<Data> {
        boolean onAddFinished(int errorCode, Data[] items);
    }

    public interface QueryCallback<Data> {
        boolean onQueryFinished(int errorCode, Data[] items);
    }

    public interface DeleteCallback<Data> {
        boolean onDeleteFinished(int errorCode, Data[] items);
    }

    public interface OnCheckListener {
        void onCheckFinished(boolean success);
    }

    public interface IDownloadObserver<Data> {
        void onUpdateStatus(Data data);

        void onProgress(Data data);
    }


}
