package com.duff.download.okdownload.interfaces;

import java.io.File;

/**
 * 下载监听器
 *
 * author：duff
 * version：1.0.0
 * date：2017/8/27
 */
public interface OnDownloadListener {
    void onStart(String url);

    void onError(String url, Exception e);

    void onProgress(String url, long downloadedBytes, long totalBytes, File file);

    void onSuccess(String url, File file);
}
