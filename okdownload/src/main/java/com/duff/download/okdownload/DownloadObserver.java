package com.duff.download.okdownload;

import com.duff.download.okdownload.model.DownloadInfo;

/**
 * author：duff
 * version：1.0.0
 * date：2017/8/27
 */
public abstract class DownloadObserver implements AbsDownloadManager.IDownloadObserver<DownloadInfo> {
}
