package com.duff.download.okdownload.utils;

import android.text.TextUtils;
import android.util.Log;

import com.duff.download.okdownload.DownloadConfiguration;

/**
 * 日志打印
 *
 * @author: duff
 * @date: 2019/4/2
 * @since: 1.1.0
 */
public class Logger {

    private final static boolean debug = DownloadConfiguration.debug();
    private final static String TAG = DownloadConfiguration.getTag();

    public static void d(String msg) {
        if (debug)
            Log.d(TAG, msg);
    }

    public static void d(String tag, String msg) {
        if (debug)
            Log.d(TextUtils.isEmpty(tag) ? TAG : tag, msg);
    }

    public static void e(String msg) {
        if (debug)
            Log.e(TAG, msg);
    }

    public static void e(String tag, String msg) {
        if (debug)
            Log.e(TextUtils.isEmpty(tag) ? TAG : tag, msg);
    }

    public static void i(String msg) {
        if (debug)
            Log.i(TAG, msg);
    }

    public static void i(String tag, String msg) {
        if (debug)
            Log.i(TextUtils.isEmpty(tag) ? TAG : tag, msg);
    }

    public static void w(String msg) {
        if (debug)
            Log.w(TAG, msg);
    }

    public static void w(String tag, String msg) {
        if (debug)
            Log.w(TextUtils.isEmpty(tag) ? TAG : tag, msg);
    }

}
