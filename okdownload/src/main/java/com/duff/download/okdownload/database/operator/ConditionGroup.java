package com.duff.download.okdownload.database.operator;

public class ConditionGroup extends Condition<ConditionGroup> {

    public ConditionGroup add(Condition condition) {
        if (condition != null) {
            mConditionBuilder.append("(").append(condition.getConditionString()).append(")");

            mWhereArgs.addAll(condition.getWhereArgsList());
        }
        return this;
    }
}
