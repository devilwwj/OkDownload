package com.duff.download.okdownload.utils;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.text.format.Formatter;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * author：duff
 * version：1.0.0
 * date：2017/8/27
 */
public class StorageUtils {

    public static final long LOW_STORAGE_THRESHOLD = 30 * 1024 * 1024; // 30M

    /**
     * @return SDCard 可读写
     */
    public static boolean isSdCardWritable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * @return 空闲的存储空间
     */
    public static long getAvailableStorage() {
        String storageDirectory = Environment.getExternalStorageDirectory().toString();
        try {
            StatFs stat = new StatFs(storageDirectory);
            long availableSize = ((long) stat.getAvailableBlocks() * (long) stat.getBlockSize());
            return availableSize;
        } catch (RuntimeException ex) {
            return 0;
        }
    }

    /**
     * @return 是否有足够的空间
     */
    public static boolean hasNoMoreFreeSpace() {
        return getAvailableStorage() < LOW_STORAGE_THRESHOLD;
    }

    /**
     * @return 是否有足够的空间
     */
    public static boolean hasNoMoreFreeSpace(String path) {
        return getFreeSpace(path) < LOW_STORAGE_THRESHOLD;
    }

    /**
     * @param path
     * @return 空闲的存储空间
     */
    public static long getFreeSpace(String path) {
        File file = new File(path);
        while (!file.exists()) {
            File parent = file.getParentFile();
            if (parent != null) {
                file = parent;
            }
        }
        try {
            StatFs stat = new StatFs(file.getAbsolutePath());
            long freeSpace = ((long) stat.getFreeBlocks() * (long) stat.getBlockSize());
            return freeSpace;
        } catch (RuntimeException ex) {
            return 0;
        }
    }

    /**
     * @param path
     * @return 文件或目录总大小
     */
    public static long getTotalSize(String path) {
        File file = new File(path);
        while (!file.exists()) {
            File parent = file.getParentFile();
            if (parent != null) {
                file = parent;
            }
        }
        try {
            StatFs stat = new StatFs(file.getAbsolutePath());
            long total = ((long) stat.getBlockCount() * (long) stat.getBlockSize());
            return total;
        } catch (RuntimeException ex) {
            return 0;
        }
    }

    /**
     * @param path
     * @return 空闲的存储空间
     */
    public static long getAvailableSize(String path) {
        File file = new File(path);
        while (!file.exists()) {
            File parent = file.getParentFile();
            if (parent != null) {
                file = parent;
            }
        }
        try {
            StatFs stat = new StatFs(file.getAbsolutePath());
            long total = ((long) stat.getAvailableBlocks() * (long) stat.getBlockSize());
            return total;
        } catch (RuntimeException ex) {
            return 0;
        }
    }

    /**
     * @param size
     * @return 自定义格式化存储大小
     */
    public static String formatFileSize(long size) {
        if (size / (1024 * 1024) > 0) {
            float tmpSize = (float) (size) / (float) (1024 * 1024);
            DecimalFormat df = new DecimalFormat("#.##");
            return "" + df.format(tmpSize) + "MB";
        } else if (size / 1024 > 0) {
            return "" + (size / (1024)) + "KB";
        } else
            return "" + size + "B";
    }

    /**
     * @param context
     * @param fileSize
     * @return 格式化存储大小
     */
    public static String formatFileSize(Context context, long fileSize) {
        return Formatter.formatFileSize(context, fileSize);
    }

}
