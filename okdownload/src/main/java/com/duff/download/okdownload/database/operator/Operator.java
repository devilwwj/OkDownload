package com.duff.download.okdownload.database.operator;

public abstract class Operator {

    @Override
    public String toString() {
        return super.toString();
    }

    public static class Equal extends Operator {
        @Override
        public String toString() {
            return "=?";
        }
    }

    public static class NotEqual extends Operator {
        @Override
        public String toString() {
            return "<>?";
        }
    }

    public static class Like extends Operator {
        @Override
        public String toString() {
            return " LIKE ?";
        }
    }

    public static class Between extends Operator {
        @Override
        public String toString() {
            return " BETWEEN ? AND ?";
        }
    }

    public static class In extends Operator {
        private int mCount;
        public In(int count) {
            mCount = count;
        }

        @Override
        public String toString() {
            if (mCount <= 0) {
                return "";
            }

            StringBuilder builder = new StringBuilder(" IN (");
            for (int i = 0; i < mCount - 1; i++) {
                builder.append("?,");
            }
            builder.append("?)");
            return builder.toString();
        }
    }

    public static class NotIn extends Operator {
        private int mCount;
        public NotIn(int count) {
            mCount = count;
        }

        @Override
        public String toString() {
            if (mCount <= 0) {
                return "";
            }
            StringBuilder builder = new StringBuilder(" NOT IN (");
            for (int i = 0; i < mCount - 1; i++) {
                builder.append("?,");
            }
            builder.append("?)");
            return builder.toString();
        }
    }

    public static class GreatThan extends Operator {
        @Override
        public String toString() {
            return ">?";
        }
    }

    public static class LessThan extends Operator {
        @Override
        public String toString() {
            return "<?";
        }
    }

    public static class GreatEqual extends Operator {
        @Override
        public String toString() {
            return ">=?";
        }
    }

    public static class LessEqual extends Operator {
        @Override
        public String toString() {
            return "<=?";
        }
    }

    public static class IsNull extends Operator {
        @Override
        public String toString() {
            return " IS NULL";
        }
    }

    public static class IsNotNull extends Operator {
        @Override
        public String toString() {
            return " IS NOT NULL";
        }
    }
}
