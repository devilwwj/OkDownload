package com.duff.download.okdownload.async;

import android.os.Handler;
import android.os.Looper;

/**
 * @author: duff
 * @date: 2019/3/30
 * @since: 1.0.0
 */
public abstract class Scheduler {

    protected static Handler mHandler = new Handler(Looper.getMainLooper());

    static class AsyncTask<Params, Result> implements Runnable {
        Task<Params, Result> mTask;

        public AsyncTask(Task<Params, Result> task) {
            this.mTask = task;
        }

        @Override
        public void run() {
            final Result result = mTask.doInBackground(mTask.mParams);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mTask.onPostExecute(result);
                }
            });
        }
    }

}
