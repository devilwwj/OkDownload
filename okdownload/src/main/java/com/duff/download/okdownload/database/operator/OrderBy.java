package com.duff.download.okdownload.database.operator;

public class OrderBy {

    public static enum Order {
        ASCEND("ASC"),
        DESCEND("DESC");

        private String mValue;

        private Order(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }
    }

    private String[] mColumnNames;
    private Order mOrder;

    public OrderBy(Order order, String...columnNames) {
        mColumnNames = columnNames;
        mOrder = order;
    }

    @Override
    public String toString() {
        if (mColumnNames == null || mColumnNames.length == 0) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < mColumnNames.length; i++) {
            builder.append(mColumnNames[i]);
            if (i < mColumnNames.length - 1) {
                builder.append(",");
            }
        }
        if (mOrder != null) {
            builder.append(" ").append(mOrder.getValue());
        }

        return builder.toString();
    }
}
