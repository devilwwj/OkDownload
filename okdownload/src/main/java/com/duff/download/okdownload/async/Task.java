package com.duff.download.okdownload.async;

/**
 * @author: duff
 * @date: 2019/3/30
 * @since: 1.0.0
 */
public abstract class Task<Params, Result> {
    public Params mParams;

    public Task(Params params) {
        this.mParams = params;
    }

    public abstract Result doInBackground(Params params);

    public void onPostExecute(Result result) {
    }

}
