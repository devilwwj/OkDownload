package com.duff.download.okdownload;

import android.app.Application;

/**
 * Author: duff
 * Date: 2017/11/6
 * Version: 1.0.3
 * Description: 下载配置, 在 {@link Application#onCreate()} 中配置
 */

public class DownloadConfiguration {

    public final static String DEFAULT_DB_NAME = "downloads.db";
    public final static boolean DEFAULT_DEBUG = false;
    public final static String DEFAULT_TAG = "OkDownload";
    public final static long DEFAULT_TIMEOUT = 10 * 1000;

    /**
     * 数据库名称,默认是"downloads.db"
     */
    private static String mDatabaseName;

    /**
     * Debug 开关
     */

    private static boolean mDebug;

    /**
     * Log tag
     */
    private static String mTag;

    /**
     * 超时时间
     */
    private static long mTimeOut;


    public static void init(Builder builder) {
        mDatabaseName = builder.dbName;
        mDebug = builder.debug;
        mTag = builder.tag;
        mTimeOut = builder.timeout;
    }

    public static String getDatabaseName() {
        return mDatabaseName;
    }

    public static boolean debug() {
        return mDebug;
    }

    public static String getTag() {
        return mTag;
    }

    public static long getTimeout() {
        return mTimeOut;
    }

    public static class Builder {
        String dbName;
        boolean debug;
        String tag;
        long timeout;

        public Builder() {
            this.dbName = DEFAULT_DB_NAME;
            this.debug = DEFAULT_DEBUG;
            this.tag = DEFAULT_TAG;
            this.timeout = DEFAULT_TIMEOUT;
        }

        public Builder dbName(String name) {
            this.dbName = name;
            return this;
        }

        public Builder debug(boolean debug) {
            this.debug = debug;
            return this;
        }

        public Builder tag(String tag) {
            this.tag = tag;
            return this;
        }

        public Builder timeout(long time) {
            this.timeout = time;
            return this;
        }
    }

}
