# OkDownload

### 简介
基于 OkHttp3 实现的下载器，支持多任务并发下载、断点续传、轻量级下载等基本功能

### 添加权限
```
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
```    

### 引入项目

```groovy
// 项目 build.gradle
maven { url 'https://dl.bintray.com/dingfengnupt/maven' }

// App 的 build.gradle
implementation 'com.duff.okdownload:okdownload:1.1.0'
```

### 项目集成

#### 1. 配置项
```java
public class DemoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        
        // 其他代码
        
        DownloadConfiguration.Builder builder = new DownloadConfiguration.Builder()
                .dbName("download_demo.db")
                .debug(true)
                .tag("OkDownload");
        DownloadConfiguration.init(builder);
    }

}
```

#### 2.单任务下载

```
        OkDownload okDownload = new OkDownload.Builder()
                .url(url)
                .tag("test")
                .path(path)
                .build();

        okDownload.download(new OnDownloadListener() {
            @Override
            public void onStart(String url) {
                Log.d(TAG, "onStart ... ");
            }

            @Override
            public void onError(String url, Exception e) {
                Log.d(TAG, "onError ... " + e.getMessage());
            }

            @Override
            public void onProgress(String url, long downloadedBytes, long totalBytes, File file) {
                Log.d(TAG, "onProgress ... " + downloadedBytes + "/" + totalBytes);
            }

            @Override
            public void onSuccess(String url, File file) {
                Log.d(TAG, "onSuccess ... ");
            }
        });
```

或者

```
        OkDownload.download(url, path, new OnDownloadListener() {
            @Override
            public void onStart(String url) {
                Log.d(TAG, "onStart ... ");
            }

            @Override
            public void onError(String url, Exception e) {
                Log.d(TAG, "onError ... " + e.getMessage());
            }

            @Override
            public void onProgress(String url, long downloadedBytes, long totalBytes, File file) {
                Log.d(TAG, "onProgress ... ");
            }

            @Override
            public void onSuccess(String url, File file) {
                Log.d(TAG, "onSuccess ... ");
            }
        });
```

#### 3.轻量级下载

```
        LiteDownload.download(url, path, new LiteDownloadListener() {
            @Override
            public void onError(Exception e) {
            
            }

            @Override
            public void onSuccess(File file) {
            
            }
        });
```

#### 4.多任务下载，支持断点续传

```
        DownloadInfo[] downloadInfos = new DownloadInfo[urls.length];
        for (int i = 0; i < urls.length; i++) {
            DownloadInfo info = new DownloadInfo();

            String url = urls[i];
            String fileName = Utils.getFileName(url);
            String filePath = DOWNLOAD_DIR + fileName;

            info.setUrl(urls[i]);
            info.setTag(fileName);
            info.setPath(filePath);
            info.setTaskName(fileName);

            downloadInfos[i] = info;
        }

        DownloadManager.instance(this).add(callback, downloadInfos);
```

### 状态监听

#### 注册监听
```
DownloadManager.instance(this).registerDownloadObserver(mDownloadObserver);
```

#### 取消监听
```
DownloadManager.instance(this).unRegisterDownloadObserver(mDownloadObserver);
```

### 常用API

- 添加任务
```
DownloadManager.instance(this).add(...);
```
- 暂停任务
```
DownloadManager.instance(this).pause(...);
```
- 恢复任务
```
DownloadManager.instance(this).resume(...);
```
- 删除任务
```
DownloadManager.instance(this).delete(...);
```
- 查询任务
```
 DownloadManager.instance(this).query(...);
```
- 并行任务数(建议用默认值1)
```
 DownloadManager.instance(this).setMaxRunningTask(3);
```